package cat.itb.niceuserform;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class Register extends AppCompatActivity {

    private TextInputLayout username;
    private TextInputLayout password;
    private TextInputLayout repeatPassword;
    private TextInputLayout email;
    private TextInputLayout name;
    private TextInputLayout surnames;
    private TextInputLayout birthDate;
    private CheckBox termsCheckbox;

    private TextInputEditText usernameText;
    private TextInputEditText passwordText;
    private TextInputEditText repeatPasswordText;
    private TextInputEditText emailText;
    private TextInputEditText nameText;
    private TextInputEditText surnamesText;


    private Button loginBtn;
    private Button registerBtn;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        username = findViewById(R.id.usernameField);
        password = findViewById(R.id.passwordField);
        repeatPassword = findViewById(R.id.repeatPasswordField);
        email = findViewById(R.id.emailField);
        name = findViewById(R.id.nameField);
        surnames = findViewById(R.id.surnamesField);

        usernameText = findViewById(R.id.usernameText);
        passwordText = findViewById(R.id.passwordText);
        repeatPasswordText = findViewById(R.id.repeatPasswordText);
        emailText = findViewById(R.id.emailText);
        nameText = findViewById(R.id.nameText);
        surnamesText = findViewById(R.id.surnamesText);

        termsCheckbox = findViewById(R.id.termsCheckbox);

        loginBtn = findViewById(R.id.login_button);
        registerBtn = findViewById(R.id.register_button);


        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean username_Ok, password_Ok , repeatPassword_Ok, email_Ok, check_Ok;

                //username validation
                if(usernameText.getText().toString().isEmpty()) {
                    username.setError("This field is required");
                    username_Ok = false;
                } else {
                    username.setError(null);
                    username_Ok = true;
                }

                //password validation
                if(passwordText.getText().toString().isEmpty()) {
                    password.setError("This field is required");
                    password_Ok = false;
                } else if(passwordText.getText().length() < 8) {
                    password.setError("Minimum 8 characters");
                    password_Ok = false;
                } else {
                    password.setError(null);
                    password_Ok = true;
                }

                //repeat password validation
                if(!repeatPasswordText.getText().toString().equalsIgnoreCase(passwordText.getText().toString())) {
                    repeatPassword.setError("Passwords do not match");
                    repeatPassword_Ok = false;
                } else {
                    repeatPassword.setError(null);
                    repeatPassword_Ok = true;
                }

                //email validation
                if(emailText.getText().toString().isEmpty()) {
                    email.setError("This field is required");
                    email_Ok = false;
                } else {
                    email.setError(null);
                    email_Ok = true;
                }

                // Checkbox validation
                if(!termsCheckbox.isChecked()) {
                    termsCheckbox.setTextColor(Color.parseColor("#FF1100"));
                    check_Ok = false;
                } else {
                    check_Ok = true;
                    termsCheckbox.setTextColor(Color.parseColor("#000000"));

                }

                if(username_Ok && password_Ok && repeatPassword_Ok && email_Ok && check_Ok) {
                    startActivity(new Intent(Register.this, Login.class));
                }
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Register.this, Login.class));
            }
        });
    }
}
