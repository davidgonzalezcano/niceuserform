package cat.itb.niceuserform;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class Login extends AppCompatActivity {

    private TextInputLayout username;
    private TextInputLayout password;
    private TextInputEditText usernameText;
    private TextInputEditText passwordText;
    private Button loginBtn;
    private Button registerBtn;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username = findViewById(R.id.usernameField);
        password = findViewById(R.id.passwordField);
        usernameText = findViewById(R.id.usernameText);
        passwordText = findViewById(R.id.passwordText);
        loginBtn = findViewById(R.id.login_button);
        registerBtn = findViewById(R.id.register_button);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean username_Ok, password_Ok;

                if(usernameText.getText().toString().isEmpty()) {
                    username.setError("This field is required");
                    username_Ok = false;
                } else {
                    username.setError(null);
                    username_Ok = true;
                }

                if(passwordText.getText().toString().isEmpty()) {
                    password.setError("Incorrect password");
                    password_Ok = false;
                } else if(passwordText.getText().length() < 8) {
                    password.setError("Minimum 8 characters");
                    password_Ok = false;
                } else {
                    password.setError(null);
                    password_Ok = true;
                }

                if(username_Ok && password_Ok) {

                    startActivity(new Intent(Login.this, Welcome.class));
                }
            }
        });

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login.this, Register.class));
            }
        });

    }
}
